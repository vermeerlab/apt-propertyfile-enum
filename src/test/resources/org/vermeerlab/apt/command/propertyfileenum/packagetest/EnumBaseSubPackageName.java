package org.vermeerlab.apt.command.propertyfileenum.packagetest;

import org.vermeerlab.apt.command.propertyfileenum.annotaion.GenerateEnumFromPropertyFile;
import org.vermeerlab.apt.command.propertyfileenum.annotaion.TargetPropertyFile;

/**
 *
 * @author Yamashita,Takahiro
 */
@GenerateEnumFromPropertyFile(basePackageName = "BasePackage", subPackageName = "SubPackage2")
public class EnumBaseSubPackageName {

    @TargetPropertyFile
    final String resourceName = "resource.message9";

}
