package org.vermeerlab.apt.command.propertyfileenum;

import java.util.Locale;
import java.util.ResourceBundle.Control;
import org.vermeerlab.apt.command.propertyfileenum.annotaion.GenerateEnumFromPropertyFile;
import org.vermeerlab.apt.command.propertyfileenum.annotaion.EnumResourceControl;
import org.vermeerlab.apt.command.propertyfileenum.annotaion.EnumResourceLocale;
import org.vermeerlab.apt.command.propertyfileenum.annotaion.TargetPropertyFile;

/**
 *
 * @author Yamashita,Takahiro
 */
@GenerateEnumFromPropertyFile
public class IsNotSingle {

    @TargetPropertyFile
    final String resorce = "resource.messagecontrol";

    @EnumResourceLocale
    public Locale getLocale() {
        return Locale.ENGLISH;
    }

    @EnumResourceLocale
    public Locale getLocale2() {
        return Locale.ENGLISH;
    }

    @EnumResourceControl
    public Control getControl() {
        return Control.getNoFallbackControl(
                Control.FORMAT_DEFAULT);
    }

    @EnumResourceControl
    public Control getControl2() {
        return Control.getNoFallbackControl(
                Control.FORMAT_DEFAULT);
    }

}
