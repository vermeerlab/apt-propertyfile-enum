package org.vermeerlab.apt.command.propertyfileenum.packagetest;

import org.vermeerlab.apt.command.propertyfileenum.annotaion.GenerateEnumFromPropertyFile;
import org.vermeerlab.apt.command.propertyfileenum.annotaion.TargetPropertyFile;

/**
 *
 * @author Yamashita,Takahiro
 */
@GenerateEnumFromPropertyFile
public class SampleEnumPackage {

    @TargetPropertyFile
    final String resourceName = "resource.message6";

}
