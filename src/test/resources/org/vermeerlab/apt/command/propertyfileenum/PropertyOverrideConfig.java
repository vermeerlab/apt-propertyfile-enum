package org.vermeerlab.apt.command.propertyfileenum;

import org.vermeerlab.apt.command.propertyfileenum.annotaion.GenerateEnumFromPropertyFile;
import org.vermeerlab.apt.command.propertyfileenum.annotaion.PropertyKeyPrefix;
import org.vermeerlab.apt.command.propertyfileenum.annotaion.PropertyKeySuffix;
import org.vermeerlab.apt.command.propertyfileenum.annotaion.PropertyValueWithKey;
import org.vermeerlab.apt.command.propertyfileenum.annotaion.PropertyValueWithValueWhenException;
import org.vermeerlab.apt.command.propertyfileenum.annotaion.TargetPropertyFile;

/**
 *
 * @author Yamashita,Takahiro
 */
@GenerateEnumFromPropertyFile
public class PropertyOverrideConfig {

    @TargetPropertyFile
    @PropertyValueWithKey(false)
    @PropertyKeyPrefix("[[")
    @PropertyKeySuffix("]]")
    @PropertyValueWithValueWhenException(false)
    final String resourceName = "resource.message";

}
