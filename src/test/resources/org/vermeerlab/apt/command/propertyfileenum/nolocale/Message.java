package org.vermeerlab.apt.command.propertyfileenum.nolocale;

import java.util.Locale;
import java.util.ResourceBundle;
import javax.annotation.Generated;

/**
 *
 */
@Generated({"org.vermeerlab.apt.AnnotationProcessorController","org.vermeerlab.apt.command.propertyfileenum.GenerateEnumFromPropertyFileCommand"})
public enum Message {
    /**
     * メッセージ３
     * <p>
     * parameter count = 0
     */
    MES003_SUB("mes003.sub", 0, "メッセージ３"),

    /**
     * メッセージ００１
     * <p>
     * parameter count = 0
     */
    MSG001("msg001", 0, "メッセージ００１"),

    /**
     * メッセージ００２{0}と{1}
     * <p>
     * parameter count = 2
     */
    MSG002("msg002", 2, "メッセージ００２{0}と{1}");

    private static final String RESOURCE_BUNDLE_BASENAME = "resource.message";

    private static ResourceBundle.Control control;

    private static Locale locale;

    private final String key;

    private final Integer paramCount;

    private final String value;

    private Message(String key, Integer paramCount, String value) {
        this.key = key;
        this.paramCount = paramCount;
        this.value = value;
    }

    /**
     * ResourceBundleから値を取得する際に使用するControlを設定します.
     * <P>
     * 本設定はロケールのFallbackを設定したい場合に使用します.
     *
     * @param control ResourceBundleから値を取得する際に使用するResourceBundle.Control
     */
    public static void setStaticControl(ResourceBundle.Control control) {
        Message.control = control;
    }

    /**
     * ResourceBundleから値を取得する際に使用するControlを返却します.
     *
     * @return ResourceBundleから値を取得する際に使用するデフォルトのResourceBundle.Control
     */
    public static ResourceBundle.Control getStaticControl() {
        return Message.control;
    }

    /**
     * ResourceBundleから値を取得する際に使用するLocaleを設定します.
     *
     * @param locale ResourceBundleから値を取得する際に使用するLocale
     */
    public static void setStaticLocale(Locale locale) {
        Message.locale = locale;
    }

    /**
     * ResourceBundleから値を取得する際に使用するLocaleを返却します.
     *
     * @return ResourceBundleから値を取得する際に使用するデフォルトのLocale
     */
    public static Locale getStaticLocale() {
        return Message.locale == null ?  java.util.Locale.getDefault() : Message.locale;
    }

    /**
     * 埋め込み文字を置換した文字列を返却します.
     * <P>
     * 例外捕捉時の対応については、リソースの取得が出来なかった場合に最低限状況判別が出来うる情報を表示させるための措置です.
     * あわせて、Resource key を付与することでリソースの取得が出来ていなかったことを可視できるようにしています.<br>
     *
     * @param locale Resourceを参照する際に使用するLocale
     * @param control Resourceを参照する際に使用するControl
     * @param params 置換文字
     * @return 置換後の文字列
     * @throws IllegalArgumentException パラメータ数が一致しない場合
     */
    public String format(Locale locale, ResourceBundle.Control control, String... params) {
        java.util.Locale _locale = locale == null ? getStaticLocale() : locale;
        java.util.ResourceBundle.Control _control = control == null ? getStaticControl() : control;
        if(params.length != this.paramCount) {
            throw new IllegalArgumentException("parameter count does not match");
        }
        try {
            String _value = this.getPropertyValue(_locale, _control);
            if(this.paramCount <= 0) {
                return _value;
            }
             return java.text.MessageFormat.format(_value, (Object[]) params);
        } catch(Exception ex) {
            return this.toValueForException();
        }
    }

    /**
     * 埋め込み文字を置換した文字列を返却します.
     * <P>
     * 例外捕捉時の対応については、リソースの取得が出来なかった場合に最低限状況判別が出来うる情報を表示させるための措置です.
     * あわせて、Resource key を付与することでリソースの取得が出来ていなかったことを可視できるようにしています.<br>
     *
     * @param locale Resourceを参照する際に使用するLocale
     * @param control Resourceを参照する際に使用するControl
     * @return 置換後の文字列
     * @throws IllegalArgumentException パラメータ数が一致しない場合
     */
    public String format(Locale locale, ResourceBundle.Control control) {
        String[] empty = {};
        return this.format(locale, control, empty);
    }

    /**
     * 埋め込み文字を置換した文字列を返却します.
     * <P>
     * 例外捕捉時の対応については、リソースの取得が出来なかった場合に最低限状況判別が出来うる情報を表示させるための措置です.
     * あわせて、Resource key を付与することでリソースの取得が出来ていなかったことを可視できるようにしています.<br>
     *
     * @param locale Resourceを参照する際に使用するLocale
     * @return 置換後の文字列
     * @throws IllegalArgumentException パラメータ数が一致しない場合
     */
    public String format(Locale locale) {
        String[] empty = {};
        return this.format(locale, empty);
    }

    /**
     * 埋め込み文字を置換した文字列を返却します.
     * <P>
     * 例外捕捉時の対応については、リソースの取得が出来なかった場合に最低限状況判別が出来うる情報を表示させるための措置です.
     * あわせて、Resource key を付与することでリソースの取得が出来ていなかったことを可視できるようにしています.<br>
     *
     * @param locale Resourceを参照する際に使用するLocale
     * @param params 置換文字
     * @return 置換後の文字列
     * @throws IllegalArgumentException パラメータ数が一致しない場合
     */
    public String format(Locale locale, String... params) {
        return this.format(locale, null, params);
    }

    /**
     * 埋め込み文字の置換をした文字列を返却します.
     * <P>
     * @param params メッセージに使用する置換文字列
     * @return 埋め込み文字を置換した文字列
     */
    public String format(String... params) {
        return this.format(null, null, params);
    }

    /**
     * Propertyファイルからキーの一致した値を返却します.
     *
     * @param locale Resourceを参照する際に使用するLocale
     * @param control Resourceを参照する際に使用するControl
     * @return Property値
     */
    protected String getPropertyValue(Locale locale, ResourceBundle.Control control) {
        String _value = control ==null 
                ? java.util.ResourceBundle.getBundle(RESOURCE_BUNDLE_BASENAME, locale).getString(this.key)
                : java.util.ResourceBundle.getBundle(RESOURCE_BUNDLE_BASENAME, locale, control).getString(this.key);
        String _result = _value;
        return _result;
    }

    /**
     * 例外時の値を返却します.
     *
     * @return 例外時の値
     */
    private String toValueForException() {
        return "" + this.key + "";
    }

    /**
     * リソースの値を返却します.
     * <P>
     * 例外捕捉時の対応については、リソースの取得が出来なかった場合に最低限状況判別が出来うるメッセージを表示させるための措置です.<br>
     * あわせてメッセージIDを付与してリソースの取得が出来ていなかったことを可視できるようにしています.<br>
     * @return 当該定数に該当するリソースの値
     */
    @Override
    public String toString() {
        try {
            java.util.Locale _locale = Message.locale == null ? java.util.Locale.getDefault() : Message.locale;
            return this.getPropertyValue(_locale, Message.control);
        } catch(Exception ex) {
            return this.toValueForException();
        }
    }
}