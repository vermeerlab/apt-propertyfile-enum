package org.vermeerlab.apt.command.propertyfileenum;

import java.util.Locale;
import org.vermeerlab.apt.command.propertyfileenum.annotaion.GenerateEnumFromPropertyFile;
import org.vermeerlab.apt.command.propertyfileenum.annotaion.EnumResourceLocale;
import org.vermeerlab.apt.command.propertyfileenum.annotaion.TargetPropertyFile;

/**
 *
 * @author Yamashita,Takahiro
 */
@GenerateEnumFromPropertyFile
public class LocaleSetEnglish {

    @TargetPropertyFile
    final String resorce = "resource.message5";

    @EnumResourceLocale
    public Locale getLocale() {
        return Locale.ENGLISH;
    }
}
