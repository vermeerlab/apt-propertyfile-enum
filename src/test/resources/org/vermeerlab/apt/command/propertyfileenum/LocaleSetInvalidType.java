package org.vermeerlab.apt.command.propertyfileenum;

import org.vermeerlab.apt.command.propertyfileenum.annotaion.GenerateEnumFromPropertyFile;
import org.vermeerlab.apt.command.propertyfileenum.annotaion.EnumResourceLocale;
import org.vermeerlab.apt.command.propertyfileenum.annotaion.TargetPropertyFile;

/**
 *
 * @author Yamashita,Takahiro
 */
@GenerateEnumFromPropertyFile
public class LocaleSetInvalidType {

    @TargetPropertyFile
    final String resorce = "resource.message5";

    @EnumResourceLocale
    public String getLocale() {
        return "Locale.ROOT";
    }
}
