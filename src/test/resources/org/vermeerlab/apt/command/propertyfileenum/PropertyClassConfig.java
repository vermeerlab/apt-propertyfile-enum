package org.vermeerlab.apt.command.propertyfileenum;

import org.vermeerlab.apt.command.propertyfileenum.annotaion.GenerateEnumFromPropertyFile;
import org.vermeerlab.apt.command.propertyfileenum.annotaion.PropertyKeyPrefix;
import org.vermeerlab.apt.command.propertyfileenum.annotaion.PropertyKeySuffix;
import org.vermeerlab.apt.command.propertyfileenum.annotaion.PropertyValueWithKey;
import org.vermeerlab.apt.command.propertyfileenum.annotaion.PropertyValueWithValueWhenException;
import org.vermeerlab.apt.command.propertyfileenum.annotaion.TargetPropertyFile;

/**
 *
 * @author Yamashita,Takahiro
 */
@GenerateEnumFromPropertyFile
public class PropertyClassConfig {

    @TargetPropertyFile
    @PropertyValueWithKey
    @PropertyKeyPrefix("<<")
    @PropertyKeySuffix(">>")
    @PropertyValueWithValueWhenException
    final String resourceName = "resource.message";

}
