package org.vermeerlab.apt.command.propertyfileenum;

import java.util.Locale;
import org.vermeerlab.apt.command.propertyfileenum.annotaion.GenerateEnumFromPropertyFile;
import org.vermeerlab.apt.command.propertyfileenum.annotaion.EnumResourceControl;
import org.vermeerlab.apt.command.propertyfileenum.annotaion.TargetPropertyFile;

/**
 *
 * @author Yamashita,Takahiro
 */
@GenerateEnumFromPropertyFile
public class InValidMethodReturnType {

    @TargetPropertyFile
    final String resource = "message";

    @EnumResourceControl
    public Locale getControl() {
        return Locale.ENGLISH;
    }
}
