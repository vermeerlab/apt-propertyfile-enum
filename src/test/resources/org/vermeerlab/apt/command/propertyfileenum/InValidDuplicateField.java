package org.vermeerlab.apt.command.propertyfileenum;

import org.vermeerlab.apt.command.propertyfileenum.annotaion.GenerateEnumFromPropertyFile;
import org.vermeerlab.apt.command.propertyfileenum.annotaion.TargetPropertyFile;

/**
 *
 * @author Yamashita,Takahiro
 */
@GenerateEnumFromPropertyFile
public class InValidDuplicateField {

    @TargetPropertyFile
    final String resource = "message";

    @TargetPropertyFile
    final String resource2 = "message";
}
