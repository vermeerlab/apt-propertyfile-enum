package org.vermeerlab.apt.command.propertyfileenum;

import org.vermeerlab.apt.command.propertyfileenum.annotaion.GenerateEnumFromPropertyFile;
import org.vermeerlab.apt.command.propertyfileenum.annotaion.TargetPropertyFile;

/**
 *
 * @author Yamashita,Takahiro
 */
@GenerateEnumFromPropertyFile
public class ClassNameSetting {

    @TargetPropertyFile(className = "ClassNameSet", classNamePrefix = "Prefix", classNameSuffix = "Suffix")
    final String resourceName = "resource.message";

}
