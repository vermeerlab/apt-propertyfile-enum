package org.vermeerlab.apt.command.propertyfileenum;

import java.util.Locale;
import java.util.ResourceBundle.Control;
import org.vermeerlab.apt.command.propertyfileenum.annotaion.EnumResourceControl;
import org.vermeerlab.apt.command.propertyfileenum.annotaion.EnumResourceLocale;
import org.vermeerlab.apt.command.propertyfileenum.annotaion.GenerateEnumFromPropertyFile;
import org.vermeerlab.apt.command.propertyfileenum.annotaion.TargetPropertyFile;

/**
 *
 * @author Yamashita,Takahiro
 */
@GenerateEnumFromPropertyFile
public class ControlResourceEnumWithCode {

    @TargetPropertyFile
    final String resorce = "resource.messagecontrol";

    @EnumResourceLocale
    public Locale getLocale() {
        return Locale.ENGLISH;
    }

    @EnumResourceControl
    public Control getControl() {
        return java.util.ResourceBundle.Control.getNoFallbackControl(
                java.util.ResourceBundle.Control.FORMAT_DEFAULT);
    }
}
