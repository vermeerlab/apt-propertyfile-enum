package org.vermeerlab.apt.command.propertyfileenum;

import com.google.common.io.Resources;
import com.google.common.truth.Truth;
import com.google.testing.compile.JavaFileObjects;
import com.google.testing.compile.JavaSourceSubjectFactory;
import java.util.Locale;
import static org.hamcrest.CoreMatchers.containsString;
import org.junit.Assert;
import org.junit.Test;
import org.vermeerlab.apt.AnnotationProcessorController;
import org.vermeerlab.compiler.SourceFileReader;

/**
 *
 * @author Yamashita,Takahiro
 */
public class GenerateEnumFromPropertyFileCommandTest {

    @Test
    public void 疎通確認() {
        Truth.assert_()
                .about(JavaSourceSubjectFactory.javaSource())
                .that(JavaFileObjects.forResource(
                        Resources.getResource(
                                "org/vermeerlab/apt/command/propertyfileenum/SampleEnum.java"
                        )))
                .processedWith(new AnnotationProcessorController())
                .compilesWithoutError();
    }

    @Test
    public void 新規クラス生成() {
        Truth.assert_()
                .about(JavaSourceSubjectFactory.javaSource())
                .that(JavaFileObjects.forResource(Resources.getResource(
                        "org/vermeerlab/apt/command/propertyfileenum/SampleEnum.java"
                )))
                .processedWith(new AnnotationProcessorController())
                .compilesWithoutError()
                .and()
                .generatesSources(SourceFileReader.of(Resources.getResource(
                        "org/vermeerlab/apt/command/propertyfileenum/sampleenum/Message.java"
                )).toJavaFileObject());
    }

    @Test
    public void フィールドに対象のアノテーションが存在しない() {
        try {
            Truth.assert_()
                    .about(JavaSourceSubjectFactory.javaSource())
                    .that(JavaFileObjects.forResource(Resources.getResource(
                            "org/vermeerlab/apt/command/propertyfileenum/NoTargetField.java"
                    )))
                    .processedWith(new AnnotationProcessorController())
                    .compilesWithoutError();

        } catch (java.lang.AssertionError e) {
            Assert.assertThat(e.getMessage(), containsString(
                              "Target item is required annotated item by TargetPropertyFile ."));
        }
    }

    @Test
    public void 親パッケージ指定なし_サブパッケージ指定なし() {
        Truth.assert_()
                .about(JavaSourceSubjectFactory.javaSource())
                .that(JavaFileObjects.forResource(Resources.getResource(
                        "org/vermeerlab/apt/command/propertyfileenum/packagetest/SampleEnumPackage.java"
                )))
                .processedWith(new AnnotationProcessorController())
                .compilesWithoutError()
                .and()
                .generatesSources(SourceFileReader.of(Resources.getResource(
                        "org/vermeerlab/apt/command/propertyfileenum/packagetest/sampleenumpackage/Message6.java"
                )).toJavaFileObject());
    }

    @Test
    public void 親パッケージ指定あり_サブパッケージ指定なし() {
        Truth.assert_()
                .about(JavaSourceSubjectFactory.javaSource())
                .that(JavaFileObjects.forResource(Resources.getResource(
                        "org/vermeerlab/apt/command/propertyfileenum/packagetest/EnumBasePackageName.java"
                )))
                .processedWith(new AnnotationProcessorController())
                .compilesWithoutError()
                .and()
                .generatesSources(SourceFileReader.of(Resources.getResource(
                        "basepackage/Message7.java"
                )).toJavaFileObject());
    }

    @Test
    public void 親パッケージ指定なし_サブパッケージ指定あり() {
        Truth.assert_()
                .about(JavaSourceSubjectFactory.javaSource())
                .that(JavaFileObjects.forResource(Resources.getResource(
                        "org/vermeerlab/apt/command/propertyfileenum/packagetest/EnumSubPackageName.java"
                )))
                .processedWith(new AnnotationProcessorController())
                .compilesWithoutError()
                .and()
                .generatesSources(SourceFileReader.of(Resources.getResource(
                        "org/vermeerlab/apt/command/propertyfileenum/packagetest/subpackage/Message8.java"
                )).toJavaFileObject());
    }

    @Test
    public void 親パッケージ指定あり_サブパッケージ指定あり() {
        Truth.assert_()
                .about(JavaSourceSubjectFactory.javaSource())
                .that(JavaFileObjects.forResource(Resources.getResource(
                        "org/vermeerlab/apt/command/propertyfileenum/packagetest/EnumBaseSubPackageName.java"
                )))
                .processedWith(new AnnotationProcessorController())
                .compilesWithoutError()
                .and()
                .generatesSources(SourceFileReader.of(Resources.getResource(
                        "basepackage/subpackage2/Message9.java"
                )).toJavaFileObject());
    }

    @Test
    public void ロケールにデフォルトリソースであるROOTを指定() {
        Truth.assert_()
                .about(JavaSourceSubjectFactory.javaSource())
                .that(JavaFileObjects.forResource(Resources.getResource(
                        "org/vermeerlab/apt/command/propertyfileenum/LocaleSetSample.java"
                )))
                .processedWith(new AnnotationProcessorController(false, "/processor-command_withCodeOnly.xml"))
                .compilesWithoutError()
                .and()
                .generatesSources(SourceFileReader.of(Resources.getResource(
                        "org/vermeerlab/apt/command/propertyfileenum/localesetsample/Message3.java")
                ).toJavaFileObject());
    }

    @Test
    public void ロケール指定時にLocale_ENGLISHを指定した場合() {
        Truth.assert_()
                .about(JavaSourceSubjectFactory.javaSource())
                .that(JavaFileObjects.forResource(Resources.getResource(
                        "org/vermeerlab/apt/command/propertyfileenum/LocaleSetEnglish.java"
                )))
                .processedWith(new AnnotationProcessorController(false, "/processor-command_withCodeOnly.xml"))
                .compilesWithoutError()
                .and()
                .generatesSources(SourceFileReader.of(Resources.getResource(
                        "org/vermeerlab/apt/command/propertyfileenum/localesetenglish/Message5.java"
                )).toJavaFileObject());
    }

    @Test
    public void 型チェック_EnumResourceLocaleにLocale以外を設定したので_デフォルトロケールが指定される場合() {
        try {
            Truth.assert_()
                    .about(JavaSourceSubjectFactory.javaSource())
                    .that(JavaFileObjects.forResource(Resources.getResource(
                            "org/vermeerlab/apt/command/propertyfileenum/LocaleSetInvalidType.java"
                    )))
                    .processedWith(new AnnotationProcessorController());
        } catch (java.lang.AssertionError e) {
            Assert.assertThat(e.getMessage(), containsString(
                              "EnumResourceLocale annotated method return type must be " + Locale.class.getName() + "."));
        }
    }

    @Test
    public void コントロールを指定_fallbackを指定することでデフォルトリソースを参照する() {
        Truth.assert_()
                .about(JavaSourceSubjectFactory.javaSource())
                .that(JavaFileObjects.forResource(Resources.getResource(
                        "org/vermeerlab/apt/command/propertyfileenum/ControlResourceEnum.java"
                )))
                .processedWith(new AnnotationProcessorController(false, "/processor-command_withCodeOnly.xml"))
                .compilesWithoutError()
                .and()
                .generatesSources(SourceFileReader.of(Resources.getResource(
                        "org/vermeerlab/apt/command/propertyfileenum/controlresourceenum/Messagecontrol.java"
                )).toJavaFileObject());
    }

    @Test
    public void 検証_アノテートされた同一種類のメソッドが複数ある() {
        try {
            Truth.assert_()
                    .about(JavaSourceSubjectFactory.javaSource())
                    .that(JavaFileObjects.forResource(Resources.getResource(
                            "org/vermeerlab/apt/command/propertyfileenum/IsNotSingle.java"
                    )))
                    .processedWith(new AnnotationProcessorController())
                    .compilesWithoutError();
        } catch (java.lang.AssertionError e) {
            Assert.assertThat(e.getMessage(), containsString(
                              "EnumResourceControl can not annotate multiple items."));
            Assert.assertThat(e.getMessage(), containsString(
                              "EnumResourceLocale can not annotate multiple items."));
        }
    }

    @Test
    public void 検証_フィールド値が重複している() {
        try {
            Truth.assert_()
                    .about(JavaSourceSubjectFactory.javaSource())
                    .that(JavaFileObjects.forResource(Resources.getResource(
                            "org/vermeerlab/apt/command/propertyfileenum/IsNotFinal.java"
                    )))
                    .processedWith(new AnnotationProcessorController())
                    .compilesWithoutError();
        } catch (java.lang.AssertionError e) {
            Assert.assertThat(e.getMessage(), containsString(
                              "TargetPropertyFile annotate field modifier must be 'final' ."));
        }
    }

    @Test
    public void 検証_メソッドの戻り値の型が正しくない() {
        try {
            Truth.assert_()
                    .about(JavaSourceSubjectFactory.javaSource())
                    .that(JavaFileObjects.forResource(Resources.getResource(
                            "org/vermeerlab/apt/command/propertyfileenum/InValidMethodReturnType.java"
                    )))
                    .processedWith(new AnnotationProcessorController())
                    .compilesWithoutError();
        } catch (java.lang.AssertionError e) {
            Assert.assertThat(e.getMessage(), containsString(
                              "EnumResourceControl annotate method return type invalid. it must be java.util.ResourceBundle.Control"));
        }
    }

    @Test
    public void 検証_TargetPropertyFile_フィールド重複() {
        try {
            Truth.assert_()
                    .about(JavaSourceSubjectFactory.javaSource())
                    .that(JavaFileObjects.forResource(Resources.getResource(
                            "org/vermeerlab/apt/command/propertyfileenum/InValidDuplicateField.java"
                    )))
                    .processedWith(new AnnotationProcessorController())
                    .compilesWithoutError();
        } catch (java.lang.AssertionError e) {
            Assert.assertThat(e.getMessage(), containsString(
                              "TargetPropertyFile annotated fields are duplicate value."));
        }
    }

    @Test
    public void クラス名情報を付与() {
        Truth.assert_()
                .about(JavaSourceSubjectFactory.javaSource())
                .that(JavaFileObjects.forResource(Resources.getResource(
                        "org/vermeerlab/apt/command/propertyfileenum/ClassNameSetting.java"
                )))
                .processedWith(new AnnotationProcessorController())
                .compilesWithoutError()
                .and()
                .generatesSources(SourceFileReader.of(Resources.getResource(
                        "org/vermeerlab/apt/command/propertyfileenum/classnamesetting/PrefixClassNameSetSuffix.java"
                )).toJavaFileObject());
    }

    @Test
    public void クラスJavaDocコメントヘッダーを付与() {
        Truth.assert_()
                .about(JavaSourceSubjectFactory.javaSource())
                .that(JavaFileObjects.forResource(Resources.getResource(
                        "org/vermeerlab/apt/command/propertyfileenum/ClassJavaDoc.java"
                )))
                .processedWith(new AnnotationProcessorController())
                .compilesWithoutError()
                .and()
                .generatesSources(SourceFileReader.of(Resources.getResource(
                        "org/vermeerlab/apt/command/propertyfileenum/classjavadoc/Message.java"
                )).toJavaFileObject());
    }

    @Test
    public void XmlProperty制御を実施() {
        Truth.assert_()
                .about(JavaSourceSubjectFactory.javaSource())
                .that(JavaFileObjects.forResource(Resources.getResource(
                        "org/vermeerlab/apt/command/propertyfileenum/PropertyXmlConfig.java"
                )))
                .processedWith(new AnnotationProcessorController(false, "/processor-command_withCode.xml"))
                .compilesWithoutError()
                .and()
                .generatesSources(SourceFileReader.of(Resources.getResource(
                        "org/vermeerlab/apt/command/propertyfileenum/propertyxmlconfig/Message.java"
                )).toJavaFileObject());
    }

    @Test
    public void クラスProperty制御を実施() {
        Truth.assert_()
                .about(JavaSourceSubjectFactory.javaSource())
                .that(JavaFileObjects.forResource(Resources.getResource(
                        "org/vermeerlab/apt/command/propertyfileenum/PropertyClassConfig.java"
                )))
                .processedWith(new AnnotationProcessorController())
                .compilesWithoutError()
                .and()
                .generatesSources(SourceFileReader.of(Resources.getResource(
                        "org/vermeerlab/apt/command/propertyfileenum/propertyclassconfig/Message.java"
                )).toJavaFileObject());
    }

    @Test
    public void XmlProperty制御をClass制御で上書き実施() {
        Truth.assert_()
                .about(JavaSourceSubjectFactory.javaSource())
                .that(JavaFileObjects.forResource(Resources.getResource(
                        "org/vermeerlab/apt/command/propertyfileenum/PropertyOverrideConfig.java"
                )))
                .processedWith(new AnnotationProcessorController(false, "/processor-command_withCode.xml"))
                .compilesWithoutError()
                .and()
                .generatesSources(SourceFileReader.of(Resources.getResource(
                        "org/vermeerlab/apt/command/propertyfileenum/propertyoverrideconfig/Message.java"
                )).toJavaFileObject());
    }

    @Test
    public void XmlLocaleRoot指定() {
        Truth.assert_()
                .about(JavaSourceSubjectFactory.javaSource())
                .that(JavaFileObjects.forResource(Resources.getResource(
                        "org/vermeerlab/apt/command/propertyfileenum/LocaleRootConfig.java"
                )))
                .processedWith(new AnnotationProcessorController(false, "/processor-command_rootlocale.xml"))
                .compilesWithoutError()
                .and()
                .generatesSources(SourceFileReader.of(Resources.getResource(
                        "org/vermeerlab/apt/command/propertyfileenum/localerootconfig/MessageRoot.java"
                )).toJavaFileObject());
    }

    @Test
    public void ロケールとコントロールを指定したコードを生成コードにも反映する() {
        Truth.assert_()
                .about(JavaSourceSubjectFactory.javaSource())
                .that(JavaFileObjects.forResource(Resources.getResource(
                        "org/vermeerlab/apt/command/propertyfileenum/ControlResourceEnumWithCode.java"
                )))
                .processedWith(new AnnotationProcessorController(false, "/processor-command_withCode.xml"))
                .compilesWithoutError()
                .and()
                .generatesSources(SourceFileReader.of(Resources.getResource(
                        "org/vermeerlab/apt/command/propertyfileenum/controlresourceenumwithcode/Messagecontrol.java"
                )).toJavaFileObject());
    }

    @Test
    public void ClassJavaDocの実行コマンド追記確認() {
        Truth.assert_()
                .about(JavaSourceSubjectFactory.javaSource())
                .that(JavaFileObjects.forResource(Resources.getResource(
                        "org/vermeerlab/apt/command/propertyfileenum/ClassJavaDocXml.java"
                )))
                .processedWith(new AnnotationProcessorController(false, "/processor-command_classjavadoc.xml"))
                .compilesWithoutError()
                .and()
                .generatesSources(SourceFileReader.of(Resources.getResource(
                        "org/vermeerlab/apt/command/propertyfileenum/classjavadocxml/Message.java"
                )).toJavaFileObject());

    }

    @Test
    public void Locale指定なし() {
        Truth.assert_()
                .about(JavaSourceSubjectFactory.javaSource())
                .that(JavaFileObjects.forResource(Resources.getResource(
                        "org/vermeerlab/apt/command/propertyfileenum/NoLocale.java"
                )))
                .processedWith(new AnnotationProcessorController(false, "/processor-command_nolocale.xml"))
                .compilesWithoutError()
                .and()
                .generatesSources(SourceFileReader.of(Resources.getResource(
                        "org/vermeerlab/apt/command/propertyfileenum/nolocale/Message.java"
                )).toJavaFileObject());

    }

}
