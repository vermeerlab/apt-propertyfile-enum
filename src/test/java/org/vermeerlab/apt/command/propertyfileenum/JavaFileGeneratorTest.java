/*
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy setCode the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  Copyright © 2017 Yamashita,Takahiro
 */
package org.vermeerlab.apt.command.propertyfileenum;

import com.google.common.io.Resources;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.vermeerlab.apt.javapoet.JavaPoetFile;
import org.vermeerlab.compiler.SourceFileReader;

/**
 *
 * @author Yamashita,Takahiro
 */
public class JavaFileGeneratorTest {

    private static final String BASE_PATH = "org/vermeerlab/apt/command/propertyfileenum/tojavafile/";

    private JavaPoetFile javaPoetFile;
    Config config;

    @Before
    public void setUp() {
        javaPoetFile = JavaPoetFile.of();
        config = new Config(false, "", "", false, null, null, null, null, null, true);
    }

    @Ignore
    @Test
    public void 疎通() {
        String resourceBaseName = "resource.message";
        System.out.println(new JavaFileGenerator(javaPoetFile, null, "Message", config, resourceBaseName)
                .createJavaPoetFile()
        );
    }

    @Test(expected = java.util.MissingResourceException.class)
    public void リソースが存在しない場合は例外スロー() {
        String resourceBaseName = "resource.messagenotexist";
        new JavaFileGenerator(javaPoetFile, null, "Messagenotexist", config, resourceBaseName)
                .createJavaPoetFile();
    }

    @Test
    public void ロケールを指定_英語ロケールを参照_ロケール一致() throws IOException {
        String resourceBaseName = "resource.message2";
        Config mergeConfig = config.mergeConfig(
                new Config(false, "", "", false, Locale.ENGLISH.getLanguage(), null, null, null, null, false));
        String javaFile = new JavaFileGenerator(javaPoetFile, null, "Message2", mergeConfig, resourceBaseName)
                .createJavaPoetFile().toString();
        String after = SourceFileReader.of(Resources.getResource(BASE_PATH + "Message2.java")).toSourceCode();
        Assert.assertThat(javaFile, is(after));
    }

    @Test
    public void ロケールを指定_英語ロケールを参照_ロケール不一致() throws IOException {
        String resourceBaseName = "resource.message2";
        String javaFile = new JavaFileGenerator(javaPoetFile, null, "Message2", config, resourceBaseName)
                .createJavaPoetFile().toString();
        String after = SourceFileReader.of(Resources.getResource(BASE_PATH + "Message2.java")).toSourceCode();
        Assert.assertThat(javaFile, is(not(after)));
    }

    @Test
    public void 存在しないロケール_デフォルトロケール_日本_が優先される() throws IOException {
        String resourceBaseName = "resource.message3";
        Config mergeConfig = config.mergeConfig(
                new Config(false, "", "", false, Locale.ITALIAN.getLanguage(), null, null, null, null, false));

        String javaFile = new JavaFileGenerator(javaPoetFile, null, "Message3", mergeConfig, resourceBaseName)
                .createJavaPoetFile().toString();
        String after = SourceFileReader.of(Resources.getResource(BASE_PATH + "Message3.java")).toSourceCode();;
        Assert.assertThat(javaFile, is(after));
    }

    @Test
    public void 存在しないロケール_ControlでFallbackを指定_デフォルトリソースが優先される() throws IOException {
        String resourceBaseName = "resource.message4";
        ResourceBundle.Control control = ResourceBundle.Control.getNoFallbackControl(
                ResourceBundle.Control.FORMAT_DEFAULT);
        Config mergeConfig = config.mergeConfig(
                new Config(false, "", "", false, Locale.ITALIAN.getLanguage(), null, control, null, null, false));
        String javaFile = new JavaFileGenerator(javaPoetFile, null, "Message4", mergeConfig, resourceBaseName)
                .createJavaPoetFile().toString();
        String after = SourceFileReader.of(Resources.getResource(BASE_PATH + "Message4.java")).toSourceCode();
        Assert.assertThat(javaFile, is(after));
    }

    @Test
    public void ロケールを指定しない_Controlで優先ロケールを指定_英語を優先させる() throws IOException {
        String resourceBaseName = "resource.message5";
        ResourceBundle.Control control = new ResourceBundle.Control() {
            @Override
            public List<Locale> getCandidateLocales(
                    String baseName, Locale locale) {
                if (locale.equals(Locale.JAPAN)) {
                    return Arrays.asList(Locale.ENGLISH,
                                         locale,
                                         Locale.JAPANESE,
                                         Locale.ROOT);
                } else {
                    return super.getCandidateLocales(
                            baseName, locale);
                }
            }
        };
        Config mergeConfig = config.mergeConfig(
                new Config(false, "", "", false, null, null, control, null, null, false));
        String javaFile = new JavaFileGenerator(javaPoetFile, null, "Message5", mergeConfig, resourceBaseName)
                .createJavaPoetFile().toString();
        String after = SourceFileReader.of(Resources.getResource(BASE_PATH + "Message5.java")).toSourceCode();
        Assert.assertThat(javaFile, is(after));
    }

    @Test
    public void setConfig() {
        String resourceBaseName = "resource.messageConfig";
        config = new Config(Boolean.TRUE, "【", "】", Boolean.TRUE, "ja", "JP", null, null, null, false);
        String javaFile = new JavaFileGenerator(javaPoetFile, null, "MessageConfig", config, resourceBaseName)
                .createJavaPoetFile().toString();
        String after = SourceFileReader.of(Resources.getResource(BASE_PATH + "MessageConfig.java")).toSourceCode();
        Assert.assertThat(javaFile, is(after));
    }

}
