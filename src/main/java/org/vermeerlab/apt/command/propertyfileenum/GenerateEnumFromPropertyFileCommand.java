/*
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  Copyright © 2017 Yamashita,Takahiro
 */
package org.vermeerlab.apt.command.propertyfileenum;

import java.lang.annotation.Annotation;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.element.Element;
import org.vermeerlab.apt.command.CommandValidator;
import org.vermeerlab.apt.command.propertyfileenum.annotaion.GenerateEnumFromPropertyFile;
import org.vermeerlab.apt.config.CommandConfigScannerInterface;
import org.vermeerlab.apt.element.CategorizedElementKind;
import org.vermeerlab.apt.element.ClassElement;
import org.vermeerlab.apt.javapoet.AbstractJavaPoetProcessorCommand;
import org.vermeerlab.base.DebugUtil;

/**
 * {@link GenerateEnumFromPropertyFile} を注釈しているクラスにて指定している情報に従ってEnumクラスを生成します.
 *
 * @author Yamashita,Takahiro
 */
public class GenerateEnumFromPropertyFileCommand extends AbstractJavaPoetProcessorCommand {

    /**
     * {@inheritDoc }
     */
    @Override
    public Class<? extends Annotation> getTargetAnnotation() {
        return GenerateEnumFromPropertyFile.class;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected List<Class<? extends CommandConfigScannerInterface>> getExtentionCommandConfigScannerClasses() {
        return Arrays.asList(ConfigScanner.class);
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public void execute(ProcessingEnvironment processingEnvironment, Element element, Boolean isDebug) {
        ClassElement classElement = CategorizedElementKind.createCategorizedElement(
                processingEnvironment, element, this.getTargetAnnotation());
        DebugUtil debugUtil = new DebugUtil(isDebug);

        TargetPropertyFileConverter targetPropertyFileConverter = TargetPropertyFileConverter.of(classElement);
        EnumResourceLocaleConverter localeConverter = EnumResourceLocaleConverter.of(classElement);
        EnumResourceControlConverter controlConverter = EnumResourceControlConverter.of(classElement);

        CommandValidator.of(this.getTargetAnnotation())
                .validate(targetPropertyFileConverter, controlConverter, localeConverter);

        ConfigScanner scanner = this.getCommandConfigScanner(ConfigScanner.class).get();
        Config config = scanner.updateLocaleControlByAnnotation(localeConverter, controlConverter);

        String packageName = this.toPackageName(classElement);

        targetPropertyFileConverter.values().stream()
                .map(targetPropertyFileFieldElement -> {
                    return JavaFileGenerator.of(this, targetPropertyFileFieldElement, config, packageName).createJavaPoetFile();
                })
                .peek(javaPoetFile -> debugUtil.print(javaPoetFile.toString()))
                .forEach(javaPoetFile -> javaPoetFile.writeTo(processingEnvironment));
    }

    //
    String toPackageName(ClassElement classElement) {
        Optional<GenerateEnumFromPropertyFile> generateEnumFromPropertyFile = classElement.getAnnotation(
                GenerateEnumFromPropertyFile.class);
        String packageName = classElement.toPackageName(
                generateEnumFromPropertyFile.get().basePackageName(),
                generateEnumFromPropertyFile.get().subPackageName()
        );
        return packageName;
    }
}
