/*
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  Copyright © 2017 Yamashita,Takahiro
 */
package org.vermeerlab.apt.command.propertyfileenum;

import java.util.Iterator;
import org.vermeerlab.apt.config.CommandConfigScannerInterface;
import org.vermeerlab.apt.config.ConfigXmlReader;

/**
 * {@link GenerateEnumFromPropertyFileCommand}の実行時に使用する定義用Xmlを扱う機能を提供します.
 *
 * @author Yamashita,Takahiro
 */
public class ConfigScanner implements CommandConfigScannerInterface {

    private Config config;

    /**
     * {@inheritDoc }
     */
    @Override
    public CommandConfigScannerInterface readConfig(ConfigXmlReader configXmlReader) {
        Iterator<String> keyPrefixList
                         = configXmlReader.scanTextValue("//propertyFileEnum/propertyKey/prefix/text()").iterator();
        String keyPrefix = keyPrefixList.hasNext()
                           ? keyPrefixList.next()
                           : "";

        Iterator<String> keySuffixList
                         = configXmlReader.scanTextValue("//propertyFileEnum/propertyKey/suffix/text()").iterator();
        String keySuffix = keySuffixList.hasNext()
                           ? keySuffixList.next()
                           : "";

        Iterator<String> withKeyList
                         = configXmlReader.scanTextValue("//propertyFileEnum/withKey/text()").iterator();
        Boolean withKey = withKeyList.hasNext()
                          ? Boolean.valueOf(withKeyList.next())
                          : false;

        Iterator<String> withValueWhenExceptionList
                         = configXmlReader.scanTextValue("//propertyFileEnum/withValueWhenException/text()").iterator();
        Boolean withValueWhenException = withValueWhenExceptionList.hasNext()
                                         ? Boolean.valueOf(withValueWhenExceptionList.next())
                                         : false;

        Boolean hasLanguage = configXmlReader.hasNode("//propertyFileEnum/locale/language");
        String localeLanguage = null;
        String localeCountry = null;
        if (hasLanguage) {
            Iterator<String> localeLanguageList
                             = configXmlReader.scanTextValue("//propertyFileEnum/locale/language/text()").iterator();

            localeLanguage = localeLanguageList.hasNext()
                             ? localeLanguageList.next()
                             : "";

            Iterator<String> localeCountryList
                             = configXmlReader.scanTextValue("//propertyFileEnum/locale/country/text()").iterator();
            localeCountry = localeCountryList.hasNext()
                            ? localeCountryList.next()
                            : "";
        }

        Iterator<String> withCodeCommentDefaultLocaleList
                         = configXmlReader.scanTextValue("//propertyFileEnum/withCodeCommentDefaultLocale/text()").iterator();
        Boolean withCodeCommentDefaultLocale = withCodeCommentDefaultLocaleList.hasNext()
                                               ? Boolean.valueOf(withCodeCommentDefaultLocaleList.next())
                                               : true;

        this.config = new Config(withKey, keyPrefix, keySuffix, withValueWhenException,
                                 localeLanguage, localeCountry, null, null, null, withCodeCommentDefaultLocale);
        return this;
    }

    //
    Config updateLocaleControlByAnnotation(EnumResourceLocaleConverter localeConverter, EnumResourceControlConverter controlConverter) {
        return config.updateLocaleControlByAnnotation(localeConverter, controlConverter);
    }

}
