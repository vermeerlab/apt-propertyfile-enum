/*
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy setCode the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  Copyright © 2017 Yamashita,Takahiro
 */
package org.vermeerlab.apt.command.propertyfileenum;

import com.squareup.javapoet.ClassName;
import com.squareup.javapoet.CodeBlock;
import com.squareup.javapoet.FieldSpec;
import com.squareup.javapoet.MethodSpec;
import com.squareup.javapoet.ParameterSpec;
import com.squareup.javapoet.TypeSpec;
import java.util.Comparator;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.ResourceBundle.Control;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.lang.model.element.Modifier;
import org.vermeerlab.apt.command.propertyfileenum.annotaion.TargetPropertyFile;
import org.vermeerlab.apt.javapoet.JavaPoetFile;

/**
 * ResourceBundleからEnumクラスのJavaFileを生成します.
 * <P>
 * Control と Locale を任意で指定できます.
 *
 * @author Yamashita,Takahiro
 */
class JavaFileGenerator {

    final JavaPoetFile javaPoetFile;
    final String packageName;
    final String generateClassName;
    final Config config;
    final String resourceBaseName;

    JavaFileGenerator(JavaPoetFile javaPoetFile, String packageName, String generateClassName, Config config, String resourceBaseName) {
        this.javaPoetFile = javaPoetFile;
        this.packageName = packageName;
        this.generateClassName = generateClassName;
        this.config = config;
        this.resourceBaseName = resourceBaseName;
    }

    /**
     * インスタンスを構築します.
     *
     * @param command 実行コマンド
     * @param targetPropertyFileConverter {@link TargetPropertyFile.class}により取得した情報
     * @param config 定義情報
     * @param packageName 出力クラスのパッケージ名
     * @return 構築したインスタンス
     */
    static JavaFileGenerator of(GenerateEnumFromPropertyFileCommand command, TargetPropertyFileFieldElement targetPropertyFileFieldElement, Config config, String packageName) {
        String resourceBaseName = targetPropertyFileFieldElement.getResourceBaseName();

        JavaPoetFile javaPoetFile = JavaPoetFile.of(command,
                                                    targetPropertyFileFieldElement.getClassCommentTitle(),
                                                    resourceBaseName);

        String generateClassName = targetPropertyFileFieldElement.toClassName();
        Config mergedConfig = targetPropertyFileFieldElement.mergeConfig(config);
        return new JavaFileGenerator(javaPoetFile, packageName, generateClassName, mergedConfig, resourceBaseName);
    }

    /**
     * ResourceBundleのkeyとvalueから出力ソースコードを保持した {@link org.vermeerlab.apt.javapoet.JavaPoetFile} を作成します.
     * <P>
     * 列挙子は自然順序でソートします.<br>
     * プロパティキーの「{@literal .}」は「{@literal _}」に置換します（フィールド値不正でコンパイルエラーになるため）.
     *
     * @return 生成した {@link org.vermeerlab.apt.javapoet.JavaPoetFile}
     */
    JavaPoetFile createJavaPoetFile() {

        TypeSpec.Builder typeSpecBuilder = TypeSpec.enumBuilder(generateClassName).addModifiers(Modifier.PUBLIC);
        Pattern pattern = Pattern.compile("\\{([0-9])\\}");

        // Enum 定数
        ResourceBundle bundle = this.getBundle(resourceBaseName);
        bundle.keySet().stream()
                .sorted(Comparator.comparing(String::toString))
                .forEachOrdered(key -> {
                    String _value = bundle.getString(key);
                    Matcher matcher = pattern.matcher(_value);
                    int count = 0;
                    while (matcher.find()) {
                        count++;
                    }
                    TypeSpec param = TypeSpec
                            .anonymousClassBuilder("$S, $L, $S", key, count, _value)
                            .addJavadoc(_value + "\n<p>\n")
                            .addJavadoc("parameter count = $L\n", count)
                            .build();
                    typeSpecBuilder.addEnumConstant(this.toEnumField(key), param);
                });

        //import的な定義
        ClassName _Locale = ClassName.get("java.util", "Locale");
        ClassName _ResourceBundle = ClassName.get("java.util", "ResourceBundle");
        ClassName _Control = ClassName.get("java.util", "ResourceBundle", "Control");

        //フィールド定義
        FieldSpec resourceBundleBaseName = FieldSpec
                .builder(String.class, "RESOURCE_BUNDLE_BASENAME", Modifier.PRIVATE, Modifier.STATIC, Modifier.FINAL)
                .initializer("$S", resourceBaseName).build();

        FieldSpec fieldKey = FieldSpec.builder(String.class, "key", Modifier.PRIVATE, Modifier.FINAL).build();
        FieldSpec fieldParamCount = FieldSpec.builder(Integer.class, "paramCount", Modifier.PRIVATE, Modifier.FINAL).build();
        FieldSpec fieldValue = FieldSpec.builder(String.class, "value", Modifier.PRIVATE, Modifier.FINAL).build();

        FieldSpec fieldLocale = FieldSpec.builder(_Locale, "locale", Modifier.PRIVATE, Modifier.STATIC).build();
        FieldSpec fieldControl = FieldSpec.builder(_Control, "control", Modifier.PRIVATE, Modifier.STATIC).build();

        typeSpecBuilder
                // フィールド
                .addField(resourceBundleBaseName)
                .addField(fieldKey)
                .addField(fieldParamCount)
                .addField(fieldValue)
                .addField(fieldControl)
                .addField(fieldLocale)
                // コンストラクタ
                .addMethod(
                        MethodSpec.constructorBuilder()
                                .addModifiers(Modifier.PRIVATE)
                                .addParameter(String.class, "key")
                                .addParameter(Integer.class, "paramCount")
                                .addParameter(String.class, "value")
                                .addStatement("this.$N = key", fieldKey)
                                .addStatement("this.$N = paramCount", fieldParamCount)
                                .addStatement("this.$N = value", fieldValue)
                                .build()
                ).build();

        // static初期化
        this.staticInitializer(typeSpecBuilder, fieldLocale, fieldControl);

        // メソッド用パラメータ
        ParameterSpec paramReplaceStrings = ParameterSpec.builder(String[].class, "params").build();
        ParameterSpec paramControl = ParameterSpec.builder(Control.class, "control").build();
        ParameterSpec paramLocale = ParameterSpec.builder(Locale.class, "locale").build();

        // メソッド実装
        MethodSpec setStaticControl = MethodSpec.methodBuilder("setStaticControl")
                .addJavadoc("ResourceBundleから値を取得する際に使用するControlを設定します.\n")
                .addJavadoc("<P>\n")
                .addJavadoc("本設定はロケールのFallbackを設定したい場合に使用します.\n")
                .addJavadoc("\n")
                .addJavadoc("@param control ResourceBundleから値を取得する際に使用する$T\n", _Control)
                .addModifiers(Modifier.PUBLIC, Modifier.STATIC)
                .addParameter(paramControl)
                .addCode(CodeBlock.builder()
                        .addStatement("$N.$N = $N", generateClassName, fieldControl, paramControl)
                        .build()
                ).build();

        //
        MethodSpec getStaticControl = MethodSpec.methodBuilder("getStaticControl")
                .addJavadoc("ResourceBundleから値を取得する際に使用するControlを返却します.\n")
                .addJavadoc("\n")
                .addJavadoc("@return ResourceBundleから値を取得する際に使用するデフォルトの$T\n", _Control)
                .addModifiers(Modifier.PUBLIC, Modifier.STATIC)
                .addCode(CodeBlock.builder()
                        .addStatement("return $N.$N", generateClassName, fieldControl)
                        .build()
                )
                .returns(_Control)
                .build();

        //
        MethodSpec setStaticLocale = MethodSpec.methodBuilder("setStaticLocale")
                .addJavadoc("ResourceBundleから値を取得する際に使用するLocaleを設定します.\n")
                .addJavadoc("\n")
                .addJavadoc("@param locale ResourceBundleから値を取得する際に使用する$T\n", _Locale)
                .addModifiers(Modifier.PUBLIC, Modifier.STATIC)
                .addParameter(paramLocale)
                .addCode(CodeBlock.builder()
                        .addStatement("$N.$N = $N", generateClassName, fieldLocale, paramLocale)
                        .build()
                ).build();

        //
        MethodSpec getStaticLocale = MethodSpec.methodBuilder("getStaticLocale")
                .addJavadoc("ResourceBundleから値を取得する際に使用するLocaleを返却します.\n")
                .addJavadoc("\n")
                .addJavadoc("@return ResourceBundleから値を取得する際に使用するデフォルトの$T\n", _Locale)
                .addModifiers(Modifier.PUBLIC, Modifier.STATIC)
                .addCode(CodeBlock.builder()
                        .addStatement(
                                "return $1N.$2N == null ?  java.util.Locale.getDefault() : $1N.$2N",
                                generateClassName, fieldLocale)
                        .build()
                )
                .returns(_Locale)
                .build();

        //
        MethodSpec getPropertyValue = MethodSpec.methodBuilder("getPropertyValue")
                .addModifiers(Modifier.PROTECTED)
                .addJavadoc("Propertyファイルからキーの一致した値を返却します.\n")
                .addJavadoc("\n")
                .addJavadoc("@param locale Resourceを参照する際に使用するLocale\n")
                .addJavadoc("@param control Resourceを参照する際に使用するControl\n")
                .addJavadoc("@return Property値\n")
                .addParameter(paramLocale)
                .addParameter(paramControl)
                .addCode(
                        CodeBlock.builder()
                                .addStatement(
                                        "String _value = $1N ==null \n"
                                        + "? $2L.getBundle($3N, $4N).getString(this.$5N)\n"
                                        + ": $2L.getBundle($3N, $4N, $1N).getString(this.$5N)",
                                        paramControl, _ResourceBundle, resourceBundleBaseName, paramLocale, fieldKey)
                                .add(
                                        this.toKey(fieldKey))
                                .addStatement(
                                        "return _result")
                                .build()
                )
                .returns(String.class).build();

        //
        MethodSpec toValueForException = MethodSpec.methodBuilder("toValueForException")
                .addModifiers(Modifier.PRIVATE)
                .addJavadoc("例外時の値を返却します.\n")
                .addJavadoc("\n")
                .addJavadoc("@return 例外時の値\n")
                .addCode(
                        this.toValueForException(fieldKey, fieldValue))
                .returns(String.class).build();

        //
        MethodSpec format = MethodSpec.methodBuilder("format")
                .addJavadoc("埋め込み文字を置換した文字列を返却します.\n")
                .addJavadoc("<P>\n")
                .addJavadoc(
                        "例外捕捉時の対応については、リソースの取得が出来なかった場合に最低限状況判別が出来うる情報を表示させるための措置です.\n")
                .addJavadoc("あわせて、Resource key を付与することでリソースの取得が出来ていなかったことを可視できるようにしています.<br>\n")
                .addJavadoc("\n")
                .addJavadoc("@param locale Resourceを参照する際に使用するLocale\n")
                .addJavadoc("@param control Resourceを参照する際に使用するControl\n")
                .addJavadoc("@param params 置換文字\n")
                .addJavadoc("@return 置換後の文字列\n")
                .addJavadoc("@throws IllegalArgumentException パラメータ数が一致しない場合\n")
                .addModifiers(Modifier.PUBLIC)
                .addParameter(paramLocale)
                .addParameter(paramControl)
                .addParameter(paramReplaceStrings).varargs(true)
                .addCode(
                        CodeBlock.builder()
                                .addStatement(
                                        "$1L _locale = $2N == null ? $3N() : $2N",
                                        _Locale, paramLocale, getStaticLocale)
                                .addStatement(
                                        "$1L _control = $2N == null ? $3N() : $2N",
                                        _Control, paramControl, getStaticControl)
                                .beginControlFlow(
                                        "if($N.length != this.$N)", paramReplaceStrings, fieldParamCount)
                                .addStatement("throw new IllegalArgumentException($S)",
                                              "parameter count does not match")
                                .endControlFlow()
                                .beginControlFlow("try")
                                .addStatement("String _value = this.$N(_locale, _control)", getPropertyValue)
                                .beginControlFlow("if(this.$N <= 0)", fieldParamCount)
                                .addStatement("return _value")
                                .endControlFlow()
                                .addStatement(
                                        " return java.text.MessageFormat.format(_value, (Object[]) $N)",
                                        paramReplaceStrings)
                                .nextControlFlow("catch($T ex)", Exception.class)
                                .addStatement("return this.toValueForException()")
                                .endControlFlow()
                                .build()
                )
                .returns(String.class).build();

        //
        MethodSpec format_locale_control = MethodSpec.methodBuilder("format")
                .addJavadoc("埋め込み文字を置換した文字列を返却します.\n")
                .addJavadoc("<P>\n")
                .addJavadoc(
                        "例外捕捉時の対応については、リソースの取得が出来なかった場合に最低限状況判別が出来うる情報を表示させるための措置です.\n")
                .addJavadoc("あわせて、Resource key を付与することでリソースの取得が出来ていなかったことを可視できるようにしています.<br>\n")
                .addJavadoc("\n")
                .addJavadoc("@param locale Resourceを参照する際に使用するLocale\n")
                .addJavadoc("@param control Resourceを参照する際に使用するControl\n")
                .addJavadoc("@return 置換後の文字列\n")
                .addJavadoc("@throws IllegalArgumentException パラメータ数が一致しない場合\n")
                .addModifiers(Modifier.PUBLIC)
                .addParameter(paramLocale)
                .addParameter(paramControl)
                .addCode(
                        CodeBlock.builder()
                                .addStatement("String[] empty = {}")
                                .addStatement("return this.$N($N, $N, empty)",
                                              format, paramLocale, paramControl)
                                .build()
                )
                .returns(String.class).build();

        //
        MethodSpec format_locale = MethodSpec.methodBuilder("format")
                .addJavadoc("埋め込み文字を置換した文字列を返却します.\n")
                .addJavadoc("<P>\n")
                .addJavadoc(
                        "例外捕捉時の対応については、リソースの取得が出来なかった場合に最低限状況判別が出来うる情報を表示させるための措置です.\n")
                .addJavadoc("あわせて、Resource key を付与することでリソースの取得が出来ていなかったことを可視できるようにしています.<br>\n")
                .addJavadoc("\n")
                .addJavadoc("@param locale Resourceを参照する際に使用するLocale\n")
                .addJavadoc("@return 置換後の文字列\n")
                .addJavadoc("@throws IllegalArgumentException パラメータ数が一致しない場合\n")
                .addModifiers(Modifier.PUBLIC)
                .addParameter(paramLocale)
                .addCode(
                        CodeBlock.builder()
                                .addStatement("String[] empty = {}")
                                .addStatement("return this.$N($N, empty)", format, paramLocale)
                                .build()
                )
                .returns(String.class).build();

        //
        MethodSpec format_params_locale = MethodSpec.methodBuilder("format")
                .addJavadoc("埋め込み文字を置換した文字列を返却します.\n")
                .addJavadoc("<P>\n")
                .addJavadoc(
                        "例外捕捉時の対応については、リソースの取得が出来なかった場合に最低限状況判別が出来うる情報を表示させるための措置です.\n")
                .addJavadoc("あわせて、Resource key を付与することでリソースの取得が出来ていなかったことを可視できるようにしています.<br>\n")
                .addJavadoc("\n")
                .addJavadoc("@param locale Resourceを参照する際に使用するLocale\n")
                .addJavadoc("@param params 置換文字\n")
                .addJavadoc("@return 置換後の文字列\n")
                .addJavadoc("@throws IllegalArgumentException パラメータ数が一致しない場合\n")
                .addModifiers(Modifier.PUBLIC)
                .addParameter(paramLocale)
                .addParameter(paramReplaceStrings).varargs(true)
                .addCode(
                        CodeBlock.builder()
                                .addStatement("return this.$N($N, null, $N)",
                                              format, paramLocale, paramReplaceStrings)
                                .build()
                ).returns(String.class).build();

        //
        MethodSpec format_params = MethodSpec.methodBuilder("format")
                .addJavadoc("埋め込み文字の置換をした文字列を返却します.\n")
                .addJavadoc("<P>\n")
                .addJavadoc("@param params メッセージに使用する置換文字列\n")
                .addJavadoc("@return 埋め込み文字を置換した文字列\n")
                .addModifiers(Modifier.PUBLIC)
                .addParameter(paramReplaceStrings).varargs(true)
                .addCode(
                        CodeBlock.builder()
                                .addStatement("return this.$N(null, null, $N)",
                                              format, paramReplaceStrings)
                                .build()
                )
                .returns(String.class).build();

        //
        MethodSpec toString = MethodSpec.methodBuilder("toString")
                .addAnnotation(Override.class)
                .addJavadoc("リソースの値を返却します.\n")
                .addJavadoc("<P>\n")
                .addJavadoc(
                        "例外捕捉時の対応については、リソースの取得が出来なかった場合に最低限状況判別が出来うるメッセージを表示させるための措置です.<br>\n")
                .addJavadoc("あわせてメッセージIDを付与してリソースの取得が出来ていなかったことを可視できるようにしています.<br>\n")
                .addJavadoc("@return 当該定数に該当するリソースの値\n")
                .addModifiers(Modifier.PUBLIC)
                .addCode(CodeBlock.builder()
                        .beginControlFlow("try")
                        .addStatement(
                                "$1L _locale = $2N.$3N == null ? $1L.getDefault() : $2N.$3N",
                                _Locale, generateClassName, fieldLocale)
                        .addStatement(
                                "return this.$N(_locale, $N.$N)",
                                getPropertyValue, generateClassName, fieldControl)
                        .nextControlFlow("catch($T ex)", Exception.class)
                        .addStatement("return this.toValueForException()")
                        .endControlFlow()
                        .build()
                ).returns(String.class).build();

        //メソッド
        typeSpecBuilder.addMethod(setStaticControl).build();
        typeSpecBuilder.addMethod(getStaticControl).build();

        typeSpecBuilder.addMethod(setStaticLocale).build();
        typeSpecBuilder.addMethod(getStaticLocale).build();

        typeSpecBuilder.addMethod(format).build();
        typeSpecBuilder.addMethod(format_locale_control).build();
        typeSpecBuilder.addMethod(format_locale).build();
        typeSpecBuilder.addMethod(format_params_locale).build();
        typeSpecBuilder.addMethod(format_params).build();

        typeSpecBuilder.addMethod(getPropertyValue).build();
        typeSpecBuilder.addMethod(toValueForException).build();
        typeSpecBuilder.addMethod(toString).build();

        return this.javaPoetFile.setCode(this.packageName, typeSpecBuilder);
    }

    /**
     * パスからResourceBundleを取得して返却します.
     *
     * @param resourceBaseName 参照元のリソースのベース名
     * @return 取得したResourceBundle
     */
    ResourceBundle getBundle(String resourceBaseName) {
        Locale _locale = this.config.locale();
        Control _control = this.config.control();

        if (_locale == null && _control == null) {
            return ResourceBundle.getBundle(resourceBaseName);
        }
        if (_locale != null && _control == null) {
            return ResourceBundle.getBundle(resourceBaseName, _locale);
        }
        if (_locale == null && _control != null) {
            return ResourceBundle.getBundle(resourceBaseName, _control);
        }
        return ResourceBundle.getBundle(resourceBaseName, _locale, _control);
    }

    /**
     * Enumの列挙名を返却します.
     * <P>
     * リソースのプロパティキーを大文字に変換したものをEnumの列挙名として編集します.
     *
     * @return 全てを大文字にした文字列
     */
    String toEnumField(String key) {
        return key.toUpperCase(Locale.ENGLISH).replaceAll("\\.", "_");
    }

    /**
     * static初期化を追記します.
     *
     * @param typeSpecBuilder 編集するコードビルダー
     */
    void staticInitializer(TypeSpec.Builder typeSpecBuilder, FieldSpec fieldLocale, FieldSpec fieldControl) {
        if (this.config.hasDefaultInitializer() == false) {
            return;
        }

        CodeBlock.Builder staticInitializerBlockBuilder = CodeBlock.builder();

        if (this.config.hasDefaultLocaleSetter()) {
            MethodSpec initLocale = MethodSpec.methodBuilder("initLocale")
                    .addJavadoc("Localeの初期設定をします.\n")
                    .addModifiers(Modifier.PRIVATE)
                    .addModifiers(Modifier.STATIC)
                    .addCode(
                            this.localeCodeBlock()
                    ).returns(Locale.class).build();

            staticInitializerBlockBuilder
                    .addStatement("$N = $N()", fieldLocale, initLocale)
                    .build();

            typeSpecBuilder.addMethod(initLocale).build();
        }

        if (this.config.controlCodeBlock() != null) {
            MethodSpec initControl = MethodSpec.methodBuilder("initControl")
                    .addJavadoc("Controlの初期設定をします.\n")
                    .addJavadoc("\n")
                    .addJavadoc("@return 初期化済みのControl\n")
                    .addModifiers(Modifier.PRIVATE)
                    .addModifiers(Modifier.STATIC)
                    .addCode(
                            this.config.controlCodeBlock() + "\n"
                    ).returns(Control.class).build();

            staticInitializerBlockBuilder
                    .addStatement("$N = $N()", fieldControl, initControl).build();

            typeSpecBuilder.addMethod(initControl).build();
        }

        typeSpecBuilder.addStaticBlock(staticInitializerBlockBuilder.build());
    }

    /**
     * 出力値の{@code CodeBlock}を返却します.
     *
     * @param fieldKey KeyのFieldSpec
     * @param value Value値
     * @return 出力値
     */
    CodeBlock toKey(FieldSpec fieldKey) {
        CodeBlock.Builder builder = CodeBlock.builder();
        if (this.config.withKey()) {
            builder.addStatement("String _result = $S + this.$N + $S + _value", this.config.keyPrefix(),
                                 fieldKey, this.config.keySuffix());
        } else {
            builder.addStatement("String _result = _value");
        }
        return builder.build();
    }

    /**
     * 例外時の出力値の{@code CodeBlock}を返却します.
     *
     * @param fieldKey KeyのFieldSpec
     * @param fieldValue ValueのFieldSpec
     * @return 出力値
     */
    CodeBlock toValueForException(FieldSpec fieldKey, FieldSpec fieldValue) {
        CodeBlock.Builder builder = CodeBlock.builder();
        if (this.config.withValueWhenException()) {
            builder.addStatement("return $S + this.$N + $S + this.$N", this.config.keyPrefix(), fieldKey,
                                 this.config.keySuffix(),
                                 fieldValue);
        } else {
            builder.addStatement("return $S + this.$N + $S", this.config.keyPrefix(), fieldKey,
                                 this.config.keySuffix());
        }
        return builder.build();
    }

    //
    CodeBlock localeCodeBlock() {
        CodeBlock.Builder builder = CodeBlock.builder();
        if (this.config.localeCodeBlock() != null) {
            return builder.add(this.config.localeCodeBlock()).build();
        }

        String _localeLanguage = this.config.toLocaleLanguage();
        String _localeCountry = this.config.toLocaleCountry();
        return builder.addStatement("return new java.util.Locale($S, $S)", _localeLanguage, _localeCountry).build();
    }
}
