/*
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  Copyright © 2017 Yamashita,Takahiro
 */
package org.vermeerlab.apt.command.propertyfileenum;

import java.util.ResourceBundle.Control;
import javax.lang.model.element.ElementKind;
import org.vermeerlab.apt.ValidationResult;
import org.vermeerlab.apt.command.ValidationInterface;
import org.vermeerlab.apt.command.propertyfileenum.annotaion.EnumResourceControl;
import org.vermeerlab.apt.element.ClassElement;
import org.vermeerlab.apt.element.MethodElements;

/**
 * {@link EnumResourceControl}を注釈したメソッドから {@link java.util.ResourceBundle.Control} 情報を取得し、
 * Enumクラス作成に使用するリソース取得時に適用できるようにします.
 *
 * @author Yamashita,Takahiro
 */
class EnumResourceControlConverter implements ValidationInterface {

    final MethodElements methodElements;

    EnumResourceControlConverter(MethodElements methodElements) {
        this.methodElements = methodElements;
    }

    /**
     * {@link EnumResourceControl}で注釈したメソッドの情報からインスタンスを構築します.
     *
     * @param classElement 取得対象のルートとなるクラス分類Element
     * @return 構築したインスタンス
     */
    static EnumResourceControlConverter of(ClassElement classElement) {
        MethodElements methodElements = classElement.children(EnumResourceControl.class).filter(ElementKind.METHOD);
        return new EnumResourceControlConverter(methodElements);
    }

    /**
     * {@inheritDoc }
     * <P>
     * 検証条件は {@link  EnumResourceControl} を参照してください.
     */
    @Override
    public ValidationResult validate() {
        ValidationResult result = ValidationResult.create();
        if (this.methodElements.isEmpty()) {
            return result;
        }
        result.append(this.methodElements.validateTargetFieldOneOrLess());
        if (result.isValid() == false) {
            return result;
        }
        result.append(this.methodElements.validateReturnTypeIsSame(Control.class));
        return result;
    }

    /**
     * 注釈したメソッドの実行結果を返却します.
     * <P>
     * 対象となるメソッドは対象となるクラスに１つのみということが事前検証により保証されています.
     * 未指定の場合は {@code null} を返却することで{@link JavaFileGenerator}でJavaコードに変換する際、無視します.
     * デフォルトは {@code null} です.
     *
     * @return メソッドの戻り値の{@code Control} . 未指定の場合は {@code null} を返却.
     */
    Control value() {
        if (this.methodElements.isEmpty()) {
            return null;
        }
        return (Control) this.methodElements.values().get(0).value();
    }

    /**
     * 注釈したメソッドのCodeBlockを返却します.
     *
     * @return メソッドのCodeBlock
     */
    String codeBlock() {
        if (this.methodElements.isEmpty()) {
            return null;
        }
        return this.methodElements.values().get(0).getMethodBlock();
    }

}
