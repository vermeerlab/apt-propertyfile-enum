/*
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  Copyright © 2017 Yamashita,Takahiro
 */
package org.vermeerlab.apt.command.propertyfileenum.annotaion;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 生成するEnumの入力情報となるProperty fileのBaseNameを指定するフィールドに付与するアノテーションです.
 * <P>
 * 検証条件
 * <ul>
 * <li>
 * 注釈は必須です.
 * </li>
 * <li>
 * {@link GenerateEnumFromPropertyFile}で注釈されたクラスのフィールドのみ処理対象です.
 * それ以外のクラスのフィールドに指定しても無視されます.
 * </li>
 * <li>
 * 注釈するフィールドは {@code final} にしてください.
 * アノテーションの仕様により {@code final} でなければ値の取得が出来ません.
 * </li>
 * <li>
 * フィールド値は一意でなければなりません.
 * 値が重複した場合、同じリソースを参照していることになります.
 * </li>
 * <li>
 * Javaのフィールドに指定不可能な文字列がプロパティキーに存在した場合はコンパイルエラーになります.
 * </li>
 * </ul>
 * <P>
 * 生成仕様
 * <ol>
 * <li>
 * フィールド値に指定したベース名（拡張子 {@code  .properties} は不要）のリソースを参照します.
 * </li>
 * <li>
 * サブフォルダによる整理をしている場合の指定は、ResourceBundleの指定に準じます<br>
 * （例：{@code hoge.fuga.message.properties} → {@code hoge.fuga.message}）<br>
 * </li>
 * <li>
 * クラス名はリソースファイル名の先頭１文字を大文字にします<br>
 * （例：{@code message.properties} → {@code Message.java}）<br>
 * 注意事項：大文字に変換するのは CamelCaseではなく、あくまで１文字目のみです.
 * </li>
 * <li>
 * 生成クラスのパッケージは {@link org.vermeerlab.apt.command.propertyfileenum.annotaion.GenerateEnumFromPropertyFile} で指定します.
 * </li>
 * <li>
 * 複数リソースの指定が可能です. それぞれのリソース毎のクラスを作成します.
 * </li>
 * </ol>
 *
 * @see PropertyValueWithKey
 * @see PropertyKeyPrefix
 * @see PropertyKeySuffix
 * @see PropertyValueWithValueWhenException
 *
 * @author Yamashita,Takahiro
 */
@Documented
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.CLASS)
public @interface TargetPropertyFile {

    /**
     * 生成するクラス名です.
     * <P>
     * 未指定の場合は、
     * クラス名はPropertyFileの先頭１文字を大文字にします<br>
     * （例：{@code message.properties} → {@code Message.java}）<br>
     * 注意事項：大文字に変換するのは CamelCaseではなく、あくまで１文字目のみです.
     *
     * @return 指定したクラス名
     */
    public String className() default "";

    /**
     * 生成するクラス名に付与する接頭文字です.
     * <P>
     * 設定は任意です.<br>
     *
     * @return 指定した接頭文字
     */
    public String classNamePrefix() default "";

    /**
     * 生成するクラス名に付与する接尾文字です.
     * <P>
     * 設定は任意です.<br>
     *
     * @return 指定した接尾文字
     */
    public String classNameSuffix() default "";

    /**
     * 生成するクラスJavaDocのヘッダーコメントです.
     * <P>
     * 設定は任意です.
     * <P>
     * {@literal processor-command.xml}の{@literal classJavaDoc} は生成するクラス全てのコメントに適用されます.
     * 本指定により クラスのヘッダーコメントを指定すれば リソース毎の識別を設けることができます.
     *
     * @return 生成するクラスJavaDocのヘッダーコメント
     */
    public String classCommentTitle() default "";
}
