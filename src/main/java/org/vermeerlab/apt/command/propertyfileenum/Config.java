/*
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  Copyright © 2017 Yamashita,Takahiro
 */
package org.vermeerlab.apt.command.propertyfileenum;

import java.util.Locale;
import java.util.ResourceBundle.Control;

/**
 * {@link GenerateEnumFromPropertyFileCommand} で使用する定義情報です.
 *
 * @author Yamashita,Takahiro
 */
class Config {

    private final Boolean withKey;
    private final String keyPrefix;
    private final String keySuffix;
    private final Boolean withValueWhenException;

    private final Locale locale;
    private final Control control;

    private final String localeCodeBlock;
    private final String controlCodeBlock;

    private final Boolean withCodeCommentDefaultLocale;

    Config(Boolean withKey, String keyPrefix, String keySuffix, Boolean withValueWhenException,
           String localeLanguage, String localeCountry, Control control, String localeCodeBlock, String controlCodeBlock,
           Boolean withCodeCommentDefaultLocale) {
        this.withKey = withKey;
        this.keyPrefix = keyPrefix;
        this.keySuffix = keySuffix;
        this.withValueWhenException = withValueWhenException;

        Locale _locale = null;
        if (localeLanguage != null) {
            _locale = new Locale(
                    localeLanguage,
                    localeCountry != null ? localeCountry : "");
        }
        this.locale = _locale;
        this.control = control;
        this.localeCodeBlock = localeCodeBlock;
        this.controlCodeBlock = controlCodeBlock;

        this.withCodeCommentDefaultLocale = withCodeCommentDefaultLocale;
    }

    /**
     * 定義情報を合成したインスタンスを返却します.
     *
     * @param config 更新に使用する定義情報
     * @return 情報を更新した新たなインスタンス
     */
    Config mergeConfig(Config config) {
        Boolean _withKey = config.withKey != null ? config.withKey : this.withKey;
        String _keyPrefix = config.keyPrefix != null ? config.keyPrefix : this.keyPrefix;
        String _keySuffix = config.keySuffix != null ? config.keySuffix : this.keySuffix;
        Boolean _withValueWhenException = config.withValueWhenException != null ? config.withValueWhenException : this.withValueWhenException;

        Locale _locale = this.mergeLocale(config.locale);
        String _localeLanguage = this.toLocaleLanguage(_locale);
        String _localeCountry = this.toLocaleCountry(_locale);

        Control _control = this.mergeControl(config.control);

        String _localeCodeBlock = config.localeCodeBlock != null ? config.localeCodeBlock : this.localeCodeBlock;
        String _controlCodeBlock = config.controlCodeBlock != null ? config.controlCodeBlock : this.controlCodeBlock;

        Boolean _withCodeCommentDefaultLocale = config.withCodeCommentDefaultLocale != null
                                                ? config.withCodeCommentDefaultLocale : this.withCodeCommentDefaultLocale;

        return new Config(_withKey, _keyPrefix, _keySuffix, _withValueWhenException,
                          _localeLanguage, _localeCountry, _control,
                          _localeCodeBlock, _controlCodeBlock, _withCodeCommentDefaultLocale);
    }

    /**
     * {@literal Locale}と{@literal Control}を更新
     *
     * @param localeConverter Annotationから取得したLocale情報
     * @param controlConverter Annotationから取得したControl情報
     * @return
     */
    Config updateLocaleControlByAnnotation(EnumResourceLocaleConverter localeConverter, EnumResourceControlConverter controlConverter) {
        Locale localeValue = localeConverter.value();
        Locale _locale = localeValue != null ? localeValue : this.locale;
        String _localeLanguage = this.toLocaleLanguage(_locale);
        String _localeCountry = this.toLocaleCountry(_locale);

        String _localeCodeBlock = localeConverter.codeBlock();

        Control _control = this.mergeControl(controlConverter.value());

        String _controlCodeBlock = controlConverter.codeBlock();

        return new Config(withKey, keyPrefix, keySuffix, withValueWhenException,
                          _localeLanguage, _localeCountry, _control,
                          _localeCodeBlock, _controlCodeBlock, withCodeCommentDefaultLocale);
    }

    //
    String toLocaleLanguage(Locale locale) {
        return locale != null ? locale.getLanguage() : null;
    }

    //
    String toLocaleLanguage() {
        return this.locale != null ? this.locale.getLanguage() : "";
    }

    //
    String toLocaleCountry(Locale locale) {
        return locale != null ? locale.getCountry() : null;
    }

    //
    String toLocaleCountry() {
        return this.locale != null ? this.locale.getCountry() : "";
    }

    //
    Locale mergeLocale(Locale locale) {
        return locale != null ? locale : this.locale;
    }

    //
    Control mergeControl(Control control) {
        return control != null ? control : this.control;
    }

    /**
     * staticフィールドの初期値設定要素の有無を判定します.
     *
     * @return 初期値設定要素がある場合 true
     */
    Boolean hasDefaultInitializer() {
        return this.locale != null || this.localeCodeBlock() != null || this.controlCodeBlock() != null;
    }

    /**
     * staticフィールドの初期値設定要素の有無を判定します.
     *
     * @return 初期値設定要素がある場合 true
     */
    Boolean hasDefaultLocaleSetter() {
        return this.locale != null || this.localeCodeBlock() != null;
    }

    Boolean withKey() {
        return withKey;
    }

    String keyPrefix() {
        return keyPrefix;
    }

    String keySuffix() {
        return keySuffix;
    }

    Boolean withValueWhenException() {
        return withValueWhenException;
    }

    Locale locale() {
        if (this.withCodeCommentDefaultLocale) {
            return Locale.getDefault();
        }
        return this.locale;
    }

    Control control() {
        return this.control;
    }

    String localeCodeBlock() {
        return this.localeCodeBlock;
    }

    String controlCodeBlock() {
        return this.controlCodeBlock;
    }
}
