/*
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  Copyright © 2017 Yamashita,Takahiro
 */
package org.vermeerlab.apt.command.propertyfileenum;

import java.util.Locale;
import org.vermeerlab.apt.command.propertyfileenum.annotaion.PropertyKeyPrefix;
import org.vermeerlab.apt.command.propertyfileenum.annotaion.PropertyKeySuffix;
import org.vermeerlab.apt.command.propertyfileenum.annotaion.PropertyValueWithKey;
import org.vermeerlab.apt.command.propertyfileenum.annotaion.PropertyValueWithValueWhenException;
import org.vermeerlab.apt.command.propertyfileenum.annotaion.TargetPropertyFile;
import org.vermeerlab.apt.element.FieldElement;

/**
 * {@code TargetPropertyFile}でアノテーションされている{@code FieldElement} を扱うクラスです.
 *
 * @author Yamashita,Takahiro
 */
class TargetPropertyFileFieldElement extends FieldElement {

    final TargetPropertyFile targetPropertyFile;
    final Config config;

    TargetPropertyFileFieldElement(FieldElement fieldElement, TargetPropertyFile targetPropertyFile, Config config) {
        super(fieldElement);
        this.targetPropertyFile = targetPropertyFile;
        this.config = config;
    }

    /**
     * インスタンスを構築します.
     *
     * @param fieldElement 汎用の分類済みElement
     * @return 構築したインスタンス
     */
    static TargetPropertyFileFieldElement of(FieldElement fieldElement) {
        TargetPropertyFile targetPropertyFile = fieldElement.getAnnotation(TargetPropertyFile.class).get();

        Boolean withKey = fieldElement.getAnnotation(PropertyValueWithKey.class).isPresent()
                          ? fieldElement.getAnnotation(PropertyValueWithKey.class).get().value() : null;

        String keyPrefix = fieldElement.getAnnotation(PropertyKeyPrefix.class).isPresent()
                           ? fieldElement.getAnnotation(PropertyKeyPrefix.class).get().value() : null;

        String keySuffix = fieldElement.getAnnotation(PropertyKeySuffix.class).isPresent()
                           ? fieldElement.getAnnotation(PropertyKeySuffix.class).get().value() : null;

        Boolean withValueWhenException = fieldElement.getAnnotation(PropertyValueWithValueWhenException.class).isPresent()
                                         ? fieldElement.getAnnotation(PropertyValueWithValueWhenException.class).get().value() : null;

        Config config
               = new Config(withKey, keyPrefix, keySuffix, withValueWhenException, null, null, null, null, null, null);

        return new TargetPropertyFileFieldElement(fieldElement, targetPropertyFile, config);
    }

    //
    Config mergeConfig(Config baseConfig) {
        return baseConfig.mergeConfig(config);
    }

    //
    String getResourceBaseName() {
        return this.getConstantValue().get();
    }

    //
    String getClassCommentTitle() {
        return targetPropertyFile.classCommentTitle();
    }

    //
    String toClassName() {
        String prefix = targetPropertyFile.classNamePrefix();
        String suffix = targetPropertyFile.classNameSuffix();
        String base = targetPropertyFile.className().equals("")
                      ? this.toClassNameInitCap()
                      : targetPropertyFile.className();
        return prefix + base + suffix;
    }

    //
    String toClassNameInitCap() {
        String[] path = this.getResourceBaseName().split("\\.");
        String baseName = path[path.length - 1];
        return baseName.substring(0, 1).toUpperCase(Locale.ENGLISH) + baseName.substring(1);
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

}
