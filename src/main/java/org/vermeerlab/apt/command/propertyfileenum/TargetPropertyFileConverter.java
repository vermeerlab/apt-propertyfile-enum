/*
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  Copyright © 2017 Yamashita,Takahiro
 */
package org.vermeerlab.apt.command.propertyfileenum;

import java.util.List;
import javax.lang.model.element.ElementKind;
import org.vermeerlab.apt.ValidationResult;
import org.vermeerlab.apt.command.ValidationInterface;
import org.vermeerlab.apt.command.propertyfileenum.annotaion.TargetPropertyFile;
import org.vermeerlab.apt.element.ClassElement;
import org.vermeerlab.apt.element.FieldElements;

/**
 * {@link TargetPropertyFile} を注釈したフィールド値からリソースを取得します.
 *
 * @author Yamashita,Takahiro
 */
class TargetPropertyFileConverter implements ValidationInterface {

    final TargetPropertyFileFieldElements targetPropertyFileFieldElements;

    TargetPropertyFileConverter(TargetPropertyFileFieldElements fieldElements) {
        this.targetPropertyFileFieldElements = fieldElements;
    }

    /**
     * {@link TargetPropertyFile}で注釈したフィールド値からインスタンスを構築します.
     *
     * @param classElement 取得対象のルートとなるクラス分類Element
     * @return 構築したインスタンス
     */
    static TargetPropertyFileConverter of(ClassElement classElement) {
        FieldElements fieldElements = classElement.children(TargetPropertyFile.class).filter(ElementKind.FIELD);
        TargetPropertyFileFieldElements targetFieldElements = TargetPropertyFileFieldElements.of(fieldElements);
        return new TargetPropertyFileConverter(targetFieldElements);
    }

    /**
     * {@inheritDoc }
     * <P>
     * 検証条件は {@link  TargetPropertyFile} を参照してください.
     */
    @Override
    public ValidationResult validate() {
        ValidationResult result = this.targetPropertyFileFieldElements.validateRequire();
        if (result.isValid() == false) {
            return result;
        }
        return this.targetPropertyFileFieldElements.validateDuplicate();
    }

    /**
     * フィールドの分類Elementリストを返却します.
     *
     * @return フィールドの分類Elementリスト
     */
    @SuppressWarnings("unchecked") //Generic
    List<TargetPropertyFileFieldElement> values() {
        return (List<TargetPropertyFileFieldElement>) this.targetPropertyFileFieldElements.values();
    }
}
