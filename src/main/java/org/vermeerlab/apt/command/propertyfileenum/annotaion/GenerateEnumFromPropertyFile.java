/*
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  Copyright © 2017 Yamashita,Takahiro
 */
package org.vermeerlab.apt.command.propertyfileenum.annotaion;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * PropertyFileの情報からEnumを生成する設定を管理するクラスに付与するアノテーションです.
 * <P>
 * 参照対象のPropertyFileを {@link TargetPropertyFile} で指定してください.
 * <P>
 * ベースパッケージおよびサブパッケージの設定により編集されるパッケージ名は
 * {@link org.vermeerlab.apt.element.ClassElement#toPackageName(java.lang.String, java.lang.String) )}
 * の仕様に従います.
 * <ul>
 * <li>
 * {@literal processor-command.xml} {@literal <propertyFileEnum>} をリソース単位で制御
 * <P>
 * {@link PropertyValueWithKey}<br>
 * {@link PropertyKeyPrefix}<br>
 * {@link PropertyKeySuffix}<br>
 * {@link PropertyValueWithValueWhenException}<br>
 * </li>
 * <li>
 * Emunクラス作成時に参照する{@code Locale} の指定
 * <P>
 * {@link EnumResourceLocale}<br>
 * </li>
 * <li>
 * Emunクラス作成時に参照する{@code Control} の指定
 * <P>
 * {@link EnumResourceControl}<br>
 * </li>
 * </ul>
 *
 * @author Yamashita,Takahiro
 */
@Documented
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.CLASS)
public @interface GenerateEnumFromPropertyFile {

    /**
     * 生成するクラスのベースパッケージです.
     * <P>
     * 設定は任意です.<br>
     * デフォルトは注釈されたクラスのパッケージを使用します.
     *
     * @return 指定したベースパッケージ名
     */
    public String basePackageName() default "";

    /**
     * 生成するクラスのサブパッケージです.
     * <P>
     * 設定は任意です.<br>
     * デフォルトは注釈されたクラスのクラス名称を小文字にした文字列を付与します.
     *
     * @return 指定したサブパッケージ名
     */
    public String subPackageName() default "";
}
