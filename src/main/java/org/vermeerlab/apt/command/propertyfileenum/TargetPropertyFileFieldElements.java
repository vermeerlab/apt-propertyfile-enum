/*
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  Copyright © 2017 Yamashita,Takahiro
 */
package org.vermeerlab.apt.command.propertyfileenum;

import java.lang.annotation.Annotation;
import java.util.List;
import java.util.stream.Collectors;
import javax.lang.model.element.ElementKind;
import org.vermeerlab.apt.element.CategorizedElementKind;
import org.vermeerlab.apt.element.FieldElements;

/**
 * {@link  TargetPropertyFileFieldElement}リストの集約クラスです.
 *
 * @author Yamashita,Takahiro
 */
class TargetPropertyFileFieldElements extends FieldElements {

    TargetPropertyFileFieldElements(FieldElements fieldElements) {
        super(fieldElements);
    }

    /**
     * {@link  TargetPropertyFileFieldElement}リストの集約インスタンスを構築します.
     *
     * @param fieldElements Field分類Element
     * @return 構築したインスタンス
     */
    static TargetPropertyFileFieldElements of(FieldElements fieldElements) {
        List<TargetPropertyFileFieldElement> _targetElements = fieldElements.values().stream()
                .map(TargetPropertyFileFieldElement::of)
                .collect(Collectors.toList());

        Class<? extends Annotation> targetAnnotaion = fieldElements.getTargetAnnotation().get();

        FieldElements _fieldElements = CategorizedElementKind
                .createCategorizedElementsByKind(_targetElements, targetAnnotaion, ElementKind.FIELD);

        return new TargetPropertyFileFieldElements(_fieldElements);

    }
}
