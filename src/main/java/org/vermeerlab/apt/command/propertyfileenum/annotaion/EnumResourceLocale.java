/*
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  Copyright © 2017 Yamashita,Takahiro
 */
package org.vermeerlab.apt.command.propertyfileenum.annotaion;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 生成するEnumの入力情報となる Property File を取得する際に適用する
 * {@link java.util.Locale} を指定するメソッドに付与するアノテーションです.
 * <P>
 * 本指定は実装時に参照する JavaDoc に出力する値に採用されます.
 * 作成されるEnum全てに適用されます.<br>
 * 未指定の場合はデフォルトロケールを採用します. ビルド時の環境に依存したコード生成を行うので
 * 開発者の母語に応じた JavaDocコメントを出力します.
 *
 * <P>
 * 検証条件
 * <ul>
 * <li>
 * 注釈は任意です.
 * </li>
 * <li>
 * {@link GenerateEnumFromPropertyFile}で注釈されたクラスのメソッドのみ処理対象です.<br>
 * それ以外のクラスのメソッドに指定しても無視されます.
 * </li>
 * <li>
 * メソッドの戻り値の型は{@link java.util.Locale}にしてください.<br>
 * </li>
 * </ul>
 *
 * @author Yamashita,Takahiro
 */
@Documented
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.CLASS)
public @interface EnumResourceLocale {

}
